<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>
<div class="breadcrumb_area bradcam_bg_1">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcam_wrap">
                    <div class="lineDivider style5"></div>
                    <span>PRACTICE AREAS</span>
                    <h3>A range of Practice Areas</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae vitae
                    aenean tincidunt pretium quam id. Massa leo.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- practice_area::start  -->
<section class="practice_area grayBg style2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="practice_grid mb_30">
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- practice_area::start  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>