<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<div class="blogBanner_area blackOverlay">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="d-flex ">
                    <div class="section__title flex-fill mb_80">
                        <span class="subTitle theme_text">RESOURCES</span>
                        <div class="d-flex justify-content-between flex-wrap">
                            <h3 class="mb-0 text-white">Browse our Resource Center</h3>
                            <p class="f_w_400 text-white">Prepared by experienced English teachers, the texts,
                            articles and conversations are brief and appropriate</p>
                        </div>
                    </div>
                    
                </div>
                <div class="blogBradcam_info">
                    <div class="blogBradcam_info_text">
                        <a href="#" class="primary_btn">FEATURED</a>
                        <h3>8 TIPS IN CHOOSING THE RIGHT LAW
                        FIRM FOR YOUR CASE AND NEEDS</h3>
                        <p>Prepared by experienced English teachers, the texts,
                        articles and conversations are brief and appropriate.</p>
                        <div class="blogBradcam_bottom d-flex align-items-center justify-content-between">
                            <a href="#">RESOURCE</a>
                            <span>JANUARY 14, 2021</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- blog::start  -->
<section class="blog_area grayBg">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex align-items-end mb_30 flex-wrap gap_20">
                <div class="section__title flex-fill">
                    <span class="subTitle">RESOURCES</span>
                    <h3 class="mb-0">Browse our Resource Center</h3>
                </div>
                <a href="#" class="theme_line_btn">ALL ARTICLES</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="blog_widget mb_30">
                    <a href="#" class="thumb">
                        <img class="img-fluid" src="img/blog/blog_img_1.png" alt="">
                    </a>
                    <div class="blog_meta">
                        <div class="blog_meta_top">
                            <a href="#">
                            <span class="blog_meta_left">RESOURCE</span>
                            </a>
                            <span class="blog_meta_right">JANUARY 14, 2021</span>
                        </div>
                        <div class="blog_meta_inner">
                            <h4>
                                <a href="#">
                                    8 TIPS IN CHOOSING THE RIGHT LAW FIRM FOR YOUR CASE AND NEEDS
                                </a>
                            </h4>
                            <p>Prepared by experienced English teachers, the texts, articles and convers
                                ations are brief and appropriate to your level of proficiency. Take the
                                multiple-choice quiz following each text, and</p>
                            <a class="theme_underLine_btn" href="#">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="blog_widget mb_30">
                    <a href="#" class="thumb">
                        <img class="img-fluid" src="img/blog/blog_img_2.png" alt="">
                    </a>
                    <div class="blog_meta">
                        <div class="blog_meta_top">
                            <a href="#">
                                <span class="blog_meta_left">RESOURCE</span>
                            </a>
                            <span class="blog_meta_right">JANUARY 14, 2021</span>
                        </div>
                        <div class="blog_meta_inner">
                            <h4>
                                <a href="#">
                                    8 TIPS IN CHOOSING THE RIGHT LAW FIRM FOR YOUR CASE AND NEEDS
                                </a>
                            </h4>
                            <p>Prepared by experienced English teachers, the texts, articles and convers
                                ations are brief and appropriate to your level of proficiency. Take the
                                multiple-choice quiz following each text, and</p>
                            <a class="theme_underLine_btn" href="#">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="blog_widget mb_30">
                    <a href="#" class="thumb">
                        <img class="img-fluid" src="img/blog/blog_img_1.png" alt="">
                    </a>
                    <div class="blog_meta">
                        <div class="blog_meta_top">
                            <a href="#">
                            <span class="blog_meta_left">RESOURCE</span>
                            </a>
                            <span class="blog_meta_right">JANUARY 14, 2021</span>
                        </div>
                        <div class="blog_meta_inner">
                            <h4>
                                <a href="#">
                                    8 TIPS IN CHOOSING THE RIGHT LAW FIRM FOR YOUR CASE AND NEEDS
                                </a>
                            </h4>
                            <p>Prepared by experienced English teachers, the texts, articles and convers
                                ations are brief and appropriate to your level of proficiency. Take the
                                multiple-choice quiz following each text, and</p>
                            <a class="theme_underLine_btn" href="#">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="blog_widget mb_30">
                    <a href="#" class="thumb">
                        <img class="img-fluid" src="img/blog/blog_img_2.png" alt="">
                    </a>
                    <div class="blog_meta">
                        <div class="blog_meta_top">
                            <a href="#">
                                <span class="blog_meta_left">RESOURCE</span>
                            </a>
                            <span class="blog_meta_right">JANUARY 14, 2021</span>
                        </div>
                        <div class="blog_meta_inner">
                            <h4>
                                <a href="#">
                                    8 TIPS IN CHOOSING THE RIGHT LAW FIRM FOR YOUR CASE AND NEEDS
                                </a>
                            </h4>
                            <p>Prepared by experienced English teachers, the texts, articles and convers
                                ations are brief and appropriate to your level of proficiency. Take the
                                multiple-choice quiz following each text, and</p>
                            <a class="theme_underLine_btn" href="#">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="blog_widget mb_30">
                    <a href="#" class="thumb">
                        <img class="img-fluid" src="img/blog/blog_img_1.png" alt="">
                    </a>
                    <div class="blog_meta">
                        <div class="blog_meta_top">
                            <a href="#">
                            <span class="blog_meta_left">RESOURCE</span>
                            </a>
                            <span class="blog_meta_right">JANUARY 14, 2021</span>
                        </div>
                        <div class="blog_meta_inner">
                            <h4>
                                <a href="#">
                                    8 TIPS IN CHOOSING THE RIGHT LAW FIRM FOR YOUR CASE AND NEEDS
                                </a>
                            </h4>
                            <p>Prepared by experienced English teachers, the texts, articles and convers
                                ations are brief and appropriate to your level of proficiency. Take the
                                multiple-choice quiz following each text, and</p>
                            <a class="theme_underLine_btn" href="#">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="blog_widget mb_30">
                    <a href="#" class="thumb">
                        <img class="img-fluid" src="img/blog/blog_img_2.png" alt="">
                    </a>
                    <div class="blog_meta">
                        <div class="blog_meta_top">
                            <a href="#">
                                <span class="blog_meta_left">RESOURCE</span>
                            </a>
                            <span class="blog_meta_right">JANUARY 14, 2021</span>
                        </div>
                        <div class="blog_meta_inner">
                            <h4>
                                <a href="#">
                                    8 TIPS IN CHOOSING THE RIGHT LAW FIRM FOR YOUR CASE AND NEEDS
                                </a>
                            </h4>
                            <p>Prepared by experienced English teachers, the texts, articles and convers
                                ations are brief and appropriate to your level of proficiency. Take the
                                multiple-choice quiz following each text, and</p>
                            <a class="theme_underLine_btn" href="#">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- blog::end  -->


<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>