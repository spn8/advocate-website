<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>
<div class="breadcrumb_area bradcam_bg_1 style4">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcam_wrap">
                    <div class="lineDivider style5"></div>
                    <span>PAST RESULTS</span>
                    <h3>Great past results for my clients</h3>
                    <p>Prepared by experienced English teachers, the texts, articles and conversations 
                    are brief and appropriate to your level of proficiency. Take the </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="caseResult_area style3">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="row">
                    <div class="col-xl-6 col-md-6">
                        <div class="caseResult_widget mb_70">
                            <div class="thumb">
                                <img class="img-fluid" src="img/case/case_img1.png" alt="">
                            </div>
                            <div class="caseResult_meta">
                                <span>$46,000,000</span>
                                <h4>
                                    <a href="case_result_details.php">
                                    CORPORATE & COMPLIANCE
                                    </a>
                                </h4>
                                <p>Prepared by experinced English teachers, texts, articles
                                and conversations are brief and appropriate to your 
                                level of proficiency. </p>
                                <a class="theme_underLine_btn style2" href="case_result_details.php">READ MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="caseResult_widget mb_70">
                            <div class="thumb">
                                <img class="img-fluid" src="img/case/case_img2.png" alt="">
                            </div>
                            <div class="caseResult_meta">
                                <span>$46,000,000</span>
                                <h4>
                                    <a href="case_result_details.php">
                                    CORPORATE & COMPLIANCE
                                    </a>
                                </h4>
                                <p>Prepared by experinced English teachers, texts, articles
                                and conversations are brief and appropriate to your 
                                level of proficiency. </p>
                                <a class="theme_underLine_btn style2" href="case_result_details.php">READ MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="caseResult_widget mb_70">
                            <div class="thumb">
                                <img class="img-fluid" src="img/case/case_img3.png" alt="">
                            </div>
                            <div class="caseResult_meta">
                                <span>$46,000,000</span>
                                <h4>
                                    <a href="case_result_details.php">
                                    CORPORATE & COMPLIANCE
                                    </a>
                                </h4>
                                <p>Prepared by experinced English teachers, texts, articles
                                and conversations are brief and appropriate to your 
                                level of proficiency. </p>
                                <a class="theme_underLine_btn style2" href="case_result_details.php">READ MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="caseResult_widget mb_70">
                            <div class="thumb">
                                <img class="img-fluid" src="img/case/case_img4.png" alt="">
                            </div>
                            <div class="caseResult_meta">
                                <span>$46,000,000</span>
                                <h4>
                                    <a href="case_result_details.php">
                                    CORPORATE & COMPLIANCE
                                    </a>
                                </h4>
                                <p>Prepared by experinced English teachers, texts, articles
                                and conversations are brief and appropriate to your 
                                level of proficiency. </p>
                                <a class="theme_underLine_btn style2" href="case_result_details.php">READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="cta_area style2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="section__title white_text text-start d-flex flex-column justify-content-start align-items-start">
                    <div class="lineDivider style7 mb_15"></div>
                    <h3>Get a Free Consultation</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
                        pellentesque adipiscing cras nec orci lacinia amet, vulputate.</p>
                    <div class="d-flex gap_30 flex-wrap">
                        <a href="contact.php" class="primary_btn">CONTACT US</a>
                        <a href="#" class="theme_line_btn style2">(800) 567 - 1783</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="faq_area grayBg section_spacing">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="section__title flex-fill mb_50">
                    <span class="subTitle theme_text">FAQS</span>
                    <h3 class="mb-0 theme_text3">Frequently Asked Questions</h3>
                </div>
                <div class="theme_according mb_30 gridStyle" id="accordion1">
                    <div class="row">
                        <div class="col-xl-6">
                        <div class="card mb_30">
                        <div class="card-header pink_bg" id="headingFive">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">DO YOU OFFER CONSULTING FOR
                                INTERNATIONAL CASES?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseFive" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                <p>Prepared by experienced English teachers, the texts, articles
                                and conversations are brief and appropriate to your level of
                                proficiency. Take the multiple-choice quiz following each text,
                                and you'll get the results immediately. You will feel both
                                challenged and accomplished.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card mb_30">
                        <div class="card-header pink_bg" id="headingSix">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">HOW BIG IS THE TEAM ON YOUR LAW FIRM?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseSix" aria-labelledby="headingSix" data-parent="#accordion1">
                            <div class="card-body">
                                <p>Prepared by experienced English teachers, the texts, articles
                                and conversations are brief and appropriate to your level of
                                proficiency. Take the multiple-choice quiz following each text,
                                and you'll get the results immediately. You will feel both
                                challenged and accomplished.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card mb_30">
                        <div class="card-header pink_bg" id="headingSix4">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix4" aria-expanded="false" aria-controls="collapseSix">CAN I  A FREE CASE CONSULTATION?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseSix4" aria-labelledby="headingSix4" data-parent="#accordion1">
                            <div class="card-body">
                                <p>Prepared by experienced English teachers, the texts, articles
                                and conversations are brief and appropriate to your level of
                                proficiency. Take the multiple-choice quiz following each text,
                                and you'll get the results immediately. You will feel both
                                challenged and accomplished.</p>
                            </div>
                        </div>
                    </div>
                        </div>
                        <div class="col-xl-6">
                        <div class="card mb_30">
                        <div class="card-header pink_bg" id="headingSix5">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix5" aria-expanded="false" aria-controls="headingSix5">HOW LONG HAS THIS LAW FIRM BEEN ACTIVE FOR?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseSix5" aria-labelledby="headingSix5" data-parent="#accordion1">
                            <div class="card-body">
                                <p>Prepared by experienced English teachers, the texts, articles
                                and conversations are brief and appropriate to your level of
                                proficiency. Take the multiple-choice quiz following each text,
                                and you'll get the results immediately. You will feel both
                                challenged and accomplished.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card mb_30">
                        <div class="card-header pink_bg" id="headingSix6">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix6" aria-expanded="false" aria-controls="headingSix6">HOW DO I GET IN TOUCH WITH YOUR LAW FIRM?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseSix6" aria-labelledby="headingSix6" data-parent="#accordion1">
                            <div class="card-body">
                                <p>Prepared by experienced English teachers, the texts, articles
                                and conversations are brief and appropriate to your level of
                                proficiency. Take the multiple-choice quiz following each text,
                                and you'll get the results immediately. You will feel both
                                challenged and accomplished.</p>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                    
                    
                    
                </div>
            </div>
        </div>
    </div>
</section>
<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>