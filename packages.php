<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>
<div class="breadcrumb_area bradcam_bg_1 style2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcam_wrap">
                    <div class="lineDivider style5"></div>
                    <span>PACKAGES</span>
                    <h3>Available Packages</h3>
                    <p>Prepared by experienced English teachers, the texts, articles and conversations 
                    are brief and appropriate to your level of proficiency. Take the </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pricing_area grayBg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="pricing_grid">
                    <div class="single_pricing">
                        <div class="pricingHeader">
                            <div class="d-flex align-items-center">
                                <span class="f_s_18 f_w_500 text-uppercase theme_text">STANDARD</span>
                            </div>
                            <h4>$ 2,500.00 USD</h4>
                            <p>Access to 10 hours of legal consultation.</p>
                        </div>
                        <div class="pricingBody">
                            <h5>WHAT’S INCLUDED:</h5>
                            <ul>
                                <li>10 hours of legal consultation</li>
                                <li>24 hour response time</li>
                                <li>Phone or email consultation</li>
                                <li>Assistance in 6 practice areas</li>
                            </ul>
                        </div>
                        <div class="pricingBottom">
                            <a class="primary_btn w-100 text-center" href="package_single.php">
                                LEARN MORE
                            </a>
                        </div>
                    </div>
                    <div class="single_pricing">
                        <div class="pricingHeader">
                            <div class="d-flex align-items-center">
                                <span class="f_s_18 f_w_500 text-uppercase theme_text">GROWTH</span>
                            </div>
                            <h4>$ 2,500.00 USD</h4>
                            <p>Access to 10 hours of legal consultation.</p>
                        </div>
                        <div class="pricingBody">
                            <h5>WHAT’S INCLUDED:</h5>
                            <ul>
                                <li>10 hours of legal consultation</li>
                                <li>24 hour response time</li>
                                <li>Phone or email consultation</li>
                                <li>Assistance in 6 practice areas</li>
                            </ul>
                        </div>
                        <div class="pricingBottom">
                            <a class="primary_btn w-100 text-center" href="package_single.php">
                                LEARN MORE
                            </a>
                        </div>
                    </div>
                    <div class="single_pricing">
                        <div class="pricingHeader">
                            <div class="d-flex align-items-center">
                                <span class="f_s_18 f_w_500 text-uppercase theme_text">ENTERPRISE</span>
                            </div>
                            <h4>$ 2,500.00 USD</h4>
                            <p>Access to 10 hours of legal consultation.</p>
                        </div>
                        <div class="pricingBody">
                            <h5>WHAT’S INCLUDED:</h5>
                            <ul>
                                <li>10 hours of legal consultation</li>
                                <li>24 hour response time</li>
                                <li>Phone or email consultation</li>
                                <li>Assistance in 6 practice areas</li>
                            </ul>
                        </div>
                        <div class="pricingBottom">
                            <a class="primary_btn w-100 text-center" href="package_single.php">
                                LEARN MORE
                            </a>
                        </div>
                    </div>
                </div>
                <div class="prisingText d-flex flex-column justify-content-center align-items-center ">
                    <h4 class="m-0">NEED A CUSTOM SERVICE OR PACKAGE?</h4>
                    <p>Prepared by experienced English teachers, articles and conversations 
                    are brief and appropriate to your level of proficiency. </p>
                    <a href="contact.php" class="theme_line_btn">CONTACT US</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>