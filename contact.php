<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<div class="contact_details_banner bradcam_bg_2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="contactForm_wrapper ">
                    <div class="pricing_details_text">
                        <div class="section__title flex-fill mb_30">
                            <span class="subTitle theme_text">CONTACT</span>
                            <h3 class="mb-0">Get in touch</h3>
                        </div>
                        <p>Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit, sed do eiusmod tempor.</p>
                        <h4>PREFER TO GET IN TOUCH DIRECTLY?</h4>
                        <div class="contactInfo d-flex align-items-center mb_15">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25.031" height="17.6" viewBox="0 0 25.031 17.6">
                                    <g id="mail" transform="translate(0 -76)">
                                        <g id="Group_4889" data-name="Group 4889" transform="translate(0 76)">
                                        <path id="Path_5465" data-name="Path 5465" d="M22.831,76H2.2A2.2,2.2,0,0,0,0,78.2V91.4a2.2,2.2,0,0,0,2.2,2.2H22.831a2.2,2.2,0,0,0,2.2-2.2V78.2A2.2,2.2,0,0,0,22.831,76Zm-.308,1.467-8.452,8.407a2.2,2.2,0,0,1-3.113,0l-8.45-8.406ZM1.467,91.1V78.5L7.8,84.8Zm1.042,1.032,6.335-6.3,1.078,1.073a3.667,3.667,0,0,0,5.184,0l1.08-1.074,6.335,6.3ZM23.564,91.1l-6.338-6.3,6.338-6.3Z" transform="translate(0 -76)" fill="#fff"/>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <h5>contact@lawyer.com</h5>
                        </div>
                        <div class="contactInfo d-flex align-items-center">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="21.638" height="21.637" viewBox="0 0 21.638 21.637">
                                    <g id="telephone" transform="translate(-0.663 0.25)">
                                        <path id="Path_5466" data-name="Path 5466" d="M5.179,14.05a22.424,22.424,0,0,0,7.459,5.843,11.609,11.609,0,0,0,4.174,1.235c.1,0,.2.009.3.009a3.457,3.457,0,0,0,2.68-1.152.1.1,0,0,0,.018-.022,11.285,11.285,0,0,1,.845-.876c.206-.2.416-.4.618-.613a2.115,2.115,0,0,0-.009-3.149l-2.632-2.632a2.131,2.131,0,0,0-1.542-.71,2.2,2.2,0,0,0-1.559.705L13.96,14.256c-.145-.083-.293-.158-.434-.228a5.415,5.415,0,0,1-.482-.263,16.594,16.594,0,0,1-3.964-3.609,9.556,9.556,0,0,1-1.34-2.137c.412-.372.8-.762,1.169-1.143.131-.136.267-.272.4-.407a2.245,2.245,0,0,0,.727-1.577,2.228,2.228,0,0,0-.727-1.577L8.008,2.01c-.153-.153-.3-.3-.447-.455-.289-.3-.591-.6-.889-.88A2.183,2.183,0,0,0,5.131,0,2.248,2.248,0,0,0,3.571.679L1.933,2.317a3.366,3.366,0,0,0-1,2.155,8.106,8.106,0,0,0,.609,3.5A20.384,20.384,0,0,0,5.179,14.05ZM2,4.564a2.317,2.317,0,0,1,.7-1.489L4.325,1.445a1.179,1.179,0,0,1,.806-.372,1.119,1.119,0,0,1,.788.381c.293.272.569.556.867.858.149.153.3.307.455.464L8.547,4.082a1.189,1.189,0,0,1,.412.819,1.189,1.189,0,0,1-.412.819c-.136.136-.272.276-.407.412-.407.412-.788.8-1.209,1.174l-.022.022A.856.856,0,0,0,6.69,8.3c0,.013.009.022.013.035a10.308,10.308,0,0,0,1.537,2.5,17.514,17.514,0,0,0,4.222,3.845A6.27,6.27,0,0,0,13.04,15a5.415,5.415,0,0,1,.482.263l.048.026a.916.916,0,0,0,.425.109.929.929,0,0,0,.653-.3l1.638-1.638a1.159,1.159,0,0,1,.8-.39,1.1,1.1,0,0,1,.775.39L20.5,16.1a1.056,1.056,0,0,1-.013,1.651c-.184.2-.377.385-.583.583a12.613,12.613,0,0,0-.915.95,2.4,2.4,0,0,1-1.879.8c-.074,0-.153,0-.228-.009a10.546,10.546,0,0,1-3.775-1.13,21.3,21.3,0,0,1-7.1-5.562,19.511,19.511,0,0,1-3.46-5.759A7.064,7.064,0,0,1,2,4.564Z" transform="translate(0 0)" fill="#fff" stroke="#fff" stroke-width="0.5"/>
                                    </g>
                                </svg>
                            </div>
                            <h5>(212) 587 - 0127</h5>
                        </div>
                    </div>
                    <div class="contact_form">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6">
                                <label for="#" class="primary_label mb_15">FULL NAME</label>
                                <input class="primary_input3 mb_35" type="text" placeholder="WHAT'S YOUR NAME?">
                            </div>
                            <div class="col-xl-6 col-lg-6">
                                <label for="#" class="primary_label mb_15">EMAIL</label>
                                <input class="primary_input3 mb_35" type="text" placeholder="WHAT'S YOUR EMAIL?">
                            </div>
                            <div class="col-xl-6 col-lg-6">
                                <label for="#" class="primary_label mb_15">PHONE NUMBER</label>
                                <input class="primary_input3 mb_35" type="text" placeholder="+1-212-555-0127">
                            </div>
                            <div class="col-xl-6 col-lg-6">
                                <label for="#" class="primary_label mb_15">SERVICE</label>
                                <input class="primary_input3 mb_35" type="text" placeholder="EX. LABOR">
                            </div>
                            <div class="col-xl-12">
                                <label for="#" class="primary_label mb_15">MESSAGE</label>
                                <textarea class="primary_textarea w-100 mb_35" placeholder="HELLO THERE, I WOULD LIKE TO TALK ABOUT..." name="" id="" cols="30" rows="10"></textarea>
                            </div>
                            <div class="col-xl-12">
                                <button href="#" class="primary_btn text-center">SEND MESSAGE</button>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="faq_area grayBg style2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="section__title flex-fill mb_50">
                    <span class="subTitle theme_text">FAQS</span>
                    <h3 class="mb-0 theme_text3">Frequently Asked Questions</h3>
                </div>
                <div class="theme_according mb_40 gridStyle" id="accordion1">
                    <div class="row">
                        <div class="col-xl-6">
                        <div class="card mb_30">
                        <div class="card-header pink_bg" id="headingFive">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">DO YOU OFFER CONSULTING FOR
                                INTERNATIONAL CASES?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseFive" aria-labelledby="headingFive" data-parent="#accordion1">
                            <div class="card-body">
                                <p>Prepared by experienced English teachers, the texts, articles
                                and conversations are brief and appropriate to your level of
                                proficiency. Take the multiple-choice quiz following each text,
                                and you'll get the results immediately. You will feel both
                                challenged and accomplished.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card mb_30">
                        <div class="card-header pink_bg" id="headingSix">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">HOW BIG IS THE TEAM ON YOUR LAW FIRM?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseSix" aria-labelledby="headingSix" data-parent="#accordion1">
                            <div class="card-body">
                                <p>Prepared by experienced English teachers, the texts, articles
                                and conversations are brief and appropriate to your level of
                                proficiency. Take the multiple-choice quiz following each text,
                                and you'll get the results immediately. You will feel both
                                challenged and accomplished.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card mb_30">
                        <div class="card-header pink_bg" id="headingSix4">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix4" aria-expanded="false" aria-controls="collapseSix">CAN I  A FREE CASE CONSULTATION?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseSix4" aria-labelledby="headingSix4" data-parent="#accordion1">
                            <div class="card-body">
                                <p>Prepared by experienced English teachers, the texts, articles
                                and conversations are brief and appropriate to your level of
                                proficiency. Take the multiple-choice quiz following each text,
                                and you'll get the results immediately. You will feel both
                                challenged and accomplished.</p>
                            </div>
                        </div>
                    </div>
                        </div>
                        <div class="col-xl-6">
                        <div class="card mb_30">
                        <div class="card-header pink_bg" id="headingSix5">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix5" aria-expanded="false" aria-controls="headingSix5">HOW LONG HAS THIS LAW FIRM BEEN ACTIVE FOR?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseSix5" aria-labelledby="headingSix5" data-parent="#accordion1">
                            <div class="card-body">
                                <p>Prepared by experienced English teachers, the texts, articles
                                and conversations are brief and appropriate to your level of
                                proficiency. Take the multiple-choice quiz following each text,
                                and you'll get the results immediately. You will feel both
                                challenged and accomplished.</p>
                            </div>
                        </div>
                    </div>
                    <div class="card mb_30">
                        <div class="card-header pink_bg" id="headingSix6">
                            <h5 class="mb-0">
                            <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix6" aria-expanded="false" aria-controls="headingSix6">HOW DO I GET IN TOUCH WITH YOUR LAW FIRM?</button>
                            </h5>
                        </div>
                        <div class="collapse" id="collapseSix6" aria-labelledby="headingSix6" data-parent="#accordion1">
                            <div class="card-body">
                                <p>Prepared by experienced English teachers, the texts, articles
                                and conversations are brief and appropriate to your level of
                                proficiency. Take the multiple-choice quiz following each text,
                                and you'll get the results immediately. You will feel both
                                challenged and accomplished.</p>
                            </div>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-8">
                <div class="faqText text-center">
                    <h4>COULDN’T FIND THE ANSWER YOU ARE LOOKING FOR?</h4>
                    <p>Please <a href="#">get in touch</a> and we will help you to get all
                    your questions answered.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- CONTACT::START  -->
<div class="contact_section grayBg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="section__title2 white_text text-center d-flex flex-column justify-content-center align-items-center mb_40">
                    <div class="lineDivider style8  mb_5"></div>
                    <span class="subTitle mb_10">OFFICE</span>
                    <h3 class="theme_text3 max_490px">Come & visit my office</h3>
                </div>
                <div class="contactInforamtionWrapper">
                    <div class="contactInforamtionBox">
                        <div class="locationImg d-flex gap_c_15 align-items-end">
                            <div class="icon">
                                <img src="img/locationImg.svg" alt="">
                            </div>
                            <h3> NEW YORK</h3>
                        </div>
                        
                        <p>Financial District, New York,0006, USA</p>
                        <div class="contactInfo d-flex align-items-center mb_15">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="19.259" height="13.542" viewBox="0 0 19.259 13.542">
                                    <g id="mail" transform="translate(0 -76)">
                                        <g id="Group_4889" data-name="Group 4889" transform="translate(0 76)">
                                        <path id="Path_5465" data-name="Path 5465" d="M17.566,76H1.693A1.7,1.7,0,0,0,0,77.693V87.849a1.7,1.7,0,0,0,1.693,1.693H17.566a1.7,1.7,0,0,0,1.693-1.693V77.693A1.694,1.694,0,0,0,17.566,76Zm-.237,1.128-6.5,6.469a1.693,1.693,0,0,1-2.395,0l-6.5-6.468ZM1.128,87.619v-9.7L6,82.774Zm.8.794L6.8,83.57l.83.825a2.821,2.821,0,0,0,3.989,0l.831-.826,4.874,4.843Zm16.2-.794-4.876-4.845,4.876-4.851Z" transform="translate(0 -76)" fill="#6d6d6d"/>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <h5>contact@lawyer.com</h5>
                        </div>
                        <div class="contactInfo d-flex align-items-center">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="18.289" height="18.286" viewBox="0 0 18.289 18.286">
                                    <g id="telephone" transform="translate(-0.663 0.25)">
                                        <path id="Path_5466" data-name="Path 5466" d="M4.5,11.823a18.869,18.869,0,0,0,6.276,4.916,9.768,9.768,0,0,0,3.512,1.039c.085,0,.166.007.251.007a2.909,2.909,0,0,0,2.255-.969.08.08,0,0,0,.015-.018,9.5,9.5,0,0,1,.711-.737c.173-.166.35-.339.52-.516a1.78,1.78,0,0,0-.007-2.65L15.82,10.68a1.793,1.793,0,0,0-1.3-.6,1.853,1.853,0,0,0-1.312.593L11.892,12c-.122-.07-.247-.133-.365-.192a4.557,4.557,0,0,1-.405-.221A13.964,13.964,0,0,1,7.786,8.547a8.041,8.041,0,0,1-1.128-1.8c.346-.313.671-.641.984-.962.111-.114.225-.228.339-.343a1.889,1.889,0,0,0,.612-1.327A1.874,1.874,0,0,0,7.981,2.79l-1.1-1.1c-.129-.129-.251-.254-.376-.383C6.264,1.058,6.01.8,5.759.568A1.837,1.837,0,0,0,4.462,0,1.892,1.892,0,0,0,3.15.571L1.771,1.95A2.832,2.832,0,0,0,.927,3.763,6.821,6.821,0,0,0,1.44,6.711,17.153,17.153,0,0,0,4.5,11.823ZM1.827,3.84a1.949,1.949,0,0,1,.586-1.253L3.784,1.216A.992.992,0,0,1,4.462.9a.942.942,0,0,1,.663.321c.247.228.479.468.73.722.125.129.254.258.383.391l1.1,1.1a1,1,0,0,1,.346.689,1,1,0,0,1-.346.689c-.114.114-.228.232-.343.346-.343.346-.663.674-1.017.988l-.018.018a.721.721,0,0,0-.184.818c0,.011.007.018.011.029a8.674,8.674,0,0,0,1.294,2.1,14.738,14.738,0,0,0,3.553,3.236,5.276,5.276,0,0,0,.486.265,4.557,4.557,0,0,1,.405.221l.041.022a.77.77,0,0,0,.357.092.782.782,0,0,0,.549-.251l1.378-1.378A.975.975,0,0,1,14.523,11a.926.926,0,0,1,.652.328L17.4,13.544a.889.889,0,0,1-.011,1.389c-.155.166-.317.324-.49.49a10.613,10.613,0,0,0-.77.8,2.023,2.023,0,0,1-1.581.671c-.063,0-.129,0-.192-.007a8.874,8.874,0,0,1-3.177-.951A17.925,17.925,0,0,1,5.2,11.255,16.418,16.418,0,0,1,2.291,6.409,5.944,5.944,0,0,1,1.827,3.84Z" transform="translate(0 0)" fill="#6d6d6d" stroke="#6d6d6d" stroke-width="0.5"/>
                                    </g>
                                </svg>
                            </div>
                            <h5>(212) 587 - 0127</h5>
                        </div>
                    </div>
                </div>
                <div class="social_media_wrapper">
                    <h3>Follow me on Social Media</h3>
                    <div class="social_media_links d-flex justify-content-center gap_30 flex-wrap">
                        <a class="d-flex align-items-center justify-content-center" href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a class="d-flex align-items-center justify-content-center" href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a class="d-flex align-items-center justify-content-center" href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a class="d-flex align-items-center justify-content-center" href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a class="d-flex align-items-center justify-content-center" href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                        <a class="d-flex align-items-center justify-content-center" href="#">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CONTACT::END  -->


<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>