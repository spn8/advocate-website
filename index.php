<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- BANNER::START  -->
<div class="banner_area blackOverlay">
    <div class="bannerSocial_links">
        <a href="#">
            <i class="fab fa-facebook-f"></i>
        </a>
        <a href="#">
            <i class="fab fa-linkedin-in"></i>
        </a>
        <a href="#">
            <i class="fab fa-twitter"></i>
        </a>
    </div>
    <div class="container">
        <div class="row d-flex align-items-center" >
            <div class="col-lg-8">
                <div class="banner_text text-center d-flex flex-column justify-content-center align-items-center">
                    <span class="subheading theme_text">SKIPPING BAIL - SINCE 2020</span>
                    <h3>In The Halls Of Justice <span class="theme_text">The Only Justice</span> Is In The Halls.</h3>
                    <div class="lineDivider mb_30"></div>
                    <div class="d-flex align-items-center gap_r_15 gap_c_30 flex-wrap justify-content-center">
                        <a class="primary_btn" href="#">Get A Free Quote</a>
                        <a class="theme_line_btn text-white" href="#">Legal Services</a>
                    </div>
                    <div class="banner_bottom_info">
                        <div class="single_banner_info d-flex align-items-center ">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15.75" height="18.19" viewBox="0 0 15.75 18.19">
                                    <path id="location" d="M18.649,7.7A7.654,7.654,0,0,0,11,1.75h-.009A7.652,7.652,0,0,0,3.343,7.686c-1.038,4.579,1.766,8.456,4.3,10.9a4.811,4.811,0,0,0,6.7,0C16.884,16.142,19.687,12.274,18.649,7.7ZM11,12.141a2.8,2.8,0,1,1,2.8-2.8A2.795,2.795,0,0,1,11,12.141Z" transform="translate(-3.121 -1.75)" fill="#fff"/>
                                </svg>
                            </div>
                            <span>25th Legal Street, London, U.K.</span>
                        </div>
                        <div class="single_banner_info d-flex align-items-center ">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15.75" height="18.19" viewBox="0 0 15.75 18.19">
                                    <path id="location" d="M18.649,7.7A7.654,7.654,0,0,0,11,1.75h-.009A7.652,7.652,0,0,0,3.343,7.686c-1.038,4.579,1.766,8.456,4.3,10.9a4.811,4.811,0,0,0,6.7,0C16.884,16.142,19.687,12.274,18.649,7.7ZM11,12.141a2.8,2.8,0,1,1,2.8-2.8A2.795,2.795,0,0,1,11,12.141Z" transform="translate(-3.121 -1.75)" fill="#fff"/>
                                </svg>
                            </div>
                            <span>+1 855 789 - 5789</span>
                        </div>
                        <div class="single_banner_info d-flex align-items-center ">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15.75" height="18.19" viewBox="0 0 15.75 18.19">
                                    <path id="location" d="M18.649,7.7A7.654,7.654,0,0,0,11,1.75h-.009A7.652,7.652,0,0,0,3.343,7.686c-1.038,4.579,1.766,8.456,4.3,10.9a4.811,4.811,0,0,0,6.7,0C16.884,16.142,19.687,12.274,18.649,7.7ZM11,12.141a2.8,2.8,0,1,1,2.8-2.8A2.795,2.795,0,0,1,11,12.141Z" transform="translate(-3.121 -1.75)" fill="#fff"/>
                                </svg>
                            </div>
                            <span>+1 855 789 - 5789</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bannerManImg">
        <img class="img-fluid" src="img/ManThumb.png" alt="">
    </div>
</div>
<!-- BANNER::END  -->

<!-- about_area::start  -->
<section class="about_area section_spacing ">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-6">
                <div class="about_info mb_20">
                    <div class="section__title flex-fill mb_20">
                        <span class="subTitle theme_text">ABOUT US</span>
                        <h4 class="mb-0">A Law Firm With More Than 50 Years In
                        Combined Experience...</h4>
                    </div>
                    <p class="mb_10">Our firm is, in many respects, similar to the businesses in which we
                    invest. The founding partners have a personal relationship that
                    dates back to 1970 in New York.</p>
                    <p class="mb_20">Cras et nulla id lorem vulputate pulvinar eget non neque. Proin
                    feugiat justo vitae euismod fringilla. In a nunc commodo, elementum
                    metus a aliquam metus. Nulla porttitor malesuada urna non
                    convallis. </p>
                    <div class="quote_text mb_25">
                        <h5>Give Your Law Firm the Template that 
                        it deserves. This Template is ready for 
                        use right away</h5>
                    </div>
                    <p class="mb_25">Aenean tellus urna, vehicula quis quam vel, finibus sollicitudin quam
                    maecenas mollis risus eu purus faucibus efficitur.</p>
                    <div class="singletureImg">
                        <img class="img-fluid" src="img/singnature.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-xl-7 col-lg-6">
                <div class="aboutThumb mb_20">
                    <img class="img-fluid" src="img/aboutBg.png" alt="#">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- about_area::end  -->

<!-- practice_area::start  -->
<section class="practice_area section_spacing grayBg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10 ">
                <div class="section__title flex-fill mb_70">
                    <span class="subTitle">PRACTICE AREAS</span>
                    <h3 class="mb-0">A range of Practice Areas</h3>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="practice_grid mb_30">
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                    <div class="practice_widget">
                        <div class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" width="78.455" height="73.327" viewBox="0 0 78.455 73.327">
                                <g id="Group_4560" data-name="Group 4560" transform="translate(1.5 1.5)">
                                    <g id="noun-scale-1634180" transform="translate(25.385 26.554)">
                                    <path id="Path_3554" data-name="Path 3554" d="M149.379,94.607l-8.087-18.747a.991.991,0,0,0-.762-.748c-.293,0-.586.408-.762.408H129.513a6.561,6.561,0,0,0-1.769-3.687,4.489,4.489,0,0,0-6.775,0,6.561,6.561,0,0,0-1.769,3.687H108.944c-.176,0-.469-.408-.762-.408a.912.912,0,0,0-.762.679L99.333,94.539a1.05,1.05,0,0,0-.762,1.019,9.647,9.647,0,1,0,19.28.068h0a1.124,1.124,0,0,0-.82-1.155l-7.208-16.914h9.493a6.348,6.348,0,0,0,1.286,3.134,5.006,5.006,0,0,0,2.582,1.757v24.317h1.758V82.448a4.767,4.767,0,0,0,2.943-1.569,6.187,6.187,0,0,0,1.511-3.321h9.493l-7.208,16.914a1.045,1.045,0,0,0-.82,1.086,9.648,9.648,0,1,0,19.28,0,.971.971,0,0,0-.762-.951ZM115.1,94.539H101.325l6.856-16.03Zm18.518,0,6.915-16.03,6.856,16.03Z" transform="translate(-98.572 -70.291)" fill="#af864d"/>
                                    <path id="Path_3555" data-name="Path 3555" d="M252.86,457.14h15.529v2.284H252.86Z" transform="translate(-234.839 -414.272)" fill="#af864d"/>
                                    <path id="Path_3556" data-name="Path 3556" d="M292.693,394.29c-2.6,0-4.75,1.37-4.978,3.654h9.957C297.443,395.66,295.3,394.29,292.693,394.29Z" transform="translate(-266.909 -356.445)" fill="#af864d"/>
                                    </g>
                                    <path id="Path_3560" data-name="Path 3560" d="M86.528-572.093H63.286V-642.42h37.243l13.161,11.481v10.081" transform="translate(-63.286 642.42)" fill="none" stroke="#6d6d6d" stroke-linecap="round" stroke-width="3"/>
                                </g>
                            </svg>
                        </div>
                        <h3>CORPORATE & COMPLIANCE</h3>
                        <p>Prepared by experienced English
                        teachers, the texts, articles and convers
                        ations are brief and appropriate to your
                        level of proficiency.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- practice_area::start  -->

<section class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section__title white_text text-center d-flex flex-column justify-content-center align-items-center">
                    <div class="lineDivider style2 mb_30"></div>
                    <h3>Get a Free <span class="theme_text">Quote</span> </h3>
                    <p class="text-center">Prepared experienced English teachers, the texts, articles and conversations
                        are brief and appropriate to your level of proficiency.
                        Take the multiple-choice quiz following.</p>
                        <div class="d-flex gap_30 flex-wrap">
                            <a href="#" class="primary_btn">CONTACT US</a>
                            <a href="#" class="theme_line_btn text-white">(800) 567 - 1783</a>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="lawer_area section_spacing">
    <div class="container">
        <div class="row">
            <div class="col-12 mb_30">
                <div class="section__title flex-fill">
                    <span class="subTitle">Meet our Lawyer</span>
                    <h3 class="mb-0">Experienced Lawyers</h3>
                    <p>Prepared by experienced English teachers, the texts, articles and conversations.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="lawer_widget mb_30">
                    <div class="lawer_widget_thumb position-relative">
                        <img class="img-fluid" src="img/lawer/lawer_1.png" alt="">
                        <span class="lawer_tag">FOUNDER</span>
                    </div>
                    <div class="widget_team">
                        <h3>Reeva Jenkins</h3>
                        <div class="d-flex widget_team_bottom justify-content-between">
                            <div class="d-flex lawerSocialLinks flex-fill ">
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </div>
                            <a class="readmore_text" href="#">
                                [ READ MORE ]
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="lawer_widget mb_30">
                    <div class="lawer_widget_thumb position-relative">
                        <img class="img-fluid" src="img/lawer/lawer_1.png" alt="">
                        <span class="lawer_tag">FOUNDER</span>
                    </div>
                    <div class="widget_team">
                        <h3>Reeva Jenkins</h3>
                        <div class="d-flex widget_team_bottom justify-content-between">
                            <div class="d-flex lawerSocialLinks flex-fill ">
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </div>
                            <a class="readmore_text" href="#">
                                [ READ MORE ]
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="lawer_widget mb_30">
                    <div class="lawer_widget_thumb position-relative">
                        <img class="img-fluid" src="img/lawer/lawer_1.png" alt="">
                        <span class="lawer_tag">FOUNDER</span>
                    </div>
                    <div class="widget_team">
                        <h3>Reeva Jenkins</h3>
                        <div class="d-flex widget_team_bottom justify-content-between">
                            <div class="d-flex lawerSocialLinks flex-fill ">
                                <a href="#">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-twitter"></i>
                                </a>
                                <a href="#">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </div>
                            <a class="readmore_text" href="#">
                                [ READ MORE ]
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="choseArea">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-7 col-lg-6">
                <div class="choseThumb mb_30">
                    <img class="img-fluid" src="img/choseImg.png" alt="">
                </div>
            </div>
            <div class="col-xl-5 col-lg-6">
                <div class="choseInfo mb_30">
                    <div class="choseInfo_inner d-flex gap_20">
                        <div class="lineDivider style3 mt_25"></div>
                        <div class="choseInfo_innerText">
                            <h3>  Why You Choose Us</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. Parturient augue tempor amet
                            lobortis dictum ipsum sed magna. Diam eget
                            pellentesque donec.</p>
                        </div>
                    </div>
                    <div class="signature_wrap d-flex gap_20 justify-content-center ">
                        <div class="thumb">
                            <img class="img-fluid" src="img/singnature2.png" alt="">
                        </div>
                        <div class="signature_text">
                            <h4>John Simons</h4>
                            <h5 class="m-0">CEO of Justicia</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="testmonial_area position-relative">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6">
                <div class="testmonial_wrapper ">
                    <div class="testmonial_wrapper_active owl-carousel">
                        <div class="single_testmonial d-flex flex-column justify-content-center align-items-center">
                            <div class="testmonial_header">
                                <div class="thumb">
                                    <img src="img/testmonial/thumb.png" alt="">
                                </div>
                                <div class="reviewer_name">
                                    <span>DOCTOR</span>
                                    <h4 class="m-0">
                                        Rachel Ferguson
                                    </h4>
                                </div>
                            </div>
                            <div class="lineDivider style4 mb_15"></div>
                            <p class="text-center">
                            Aenean nec purus at lectus congue blandit. Ut massa augue, gravida id purus a, eleifend sodales dui.
                                Donec finibus, est ac eleifend tempus, justo nisl gravida arcu, quis aliquet ex eros vitae dui. Vivamus
                                aliquam vel ipsum eget venenatis. Vivamus aliquam vel ipsum eget venenatis.
                            </p>
                        </div>
                        <div class="single_testmonial d-flex flex-column justify-content-center align-items-center">
                            <div class="testmonial_header">
                                <div class="thumb">
                                    <img src="img/testmonial/thumb.png" alt="">
                                </div>
                                <div class="reviewer_name">
                                    <span>DOCTOR</span>
                                    <h4 class="m-0">
                                        Rachel Ferguson
                                    </h4>
                                </div>
                            </div>
                            <div class="lineDivider style4 mb_15"></div>
                            <p class="text-center">
                            Aenean nec purus at lectus congue blandit. Ut massa augue, gravida id purus a, eleifend sodales dui.
                                Donec finibus, est ac eleifend tempus, justo nisl gravida arcu, quis aliquet ex eros vitae dui. Vivamus
                                aliquam vel ipsum eget venenatis. Vivamus aliquam vel ipsum eget venenatis.
                            </p>
                        </div>
                        <div class="single_testmonial d-flex flex-column justify-content-center align-items-center">
                            <div class="testmonial_header">
                                <div class="thumb">
                                    <img src="img/testmonial/thumb.png" alt="">
                                </div>
                                <div class="reviewer_name">
                                    <span>DOCTOR</span>
                                    <h4 class="m-0">
                                        Rachel Ferguson
                                    </h4>
                                </div>
                            </div>
                            <div class="lineDivider style4 mb_15"></div>
                            <p class="text-center">
                            Aenean nec purus at lectus congue blandit. Ut massa augue, gravida id purus a, eleifend sodales dui.
                                Donec finibus, est ac eleifend tempus, justo nisl gravida arcu, quis aliquet ex eros vitae dui. Vivamus
                                aliquam vel ipsum eget venenatis. Vivamus aliquam vel ipsum eget venenatis.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="testmonial_thumb">
        <img class="img-fluid" src="img/testmonial/testimonial_thumb.png" alt="">
    </div>
</section>


<section class="brand_area">
    <div class="barnd_wrapper brand_active owl-carousel">
        <div class="single_brand">
            <img src="img/brand/1.png" alt="">
        </div>
        <div class="single_brand">
            <img src="img/brand/2.png" alt="">
        </div>
        <div class="single_brand">
            <img src="img/brand/3.png" alt="">
        </div>
        <div class="single_brand">
            <img src="img/brand/4.png" alt="">
        </div>
        <div class="single_brand">
            <img src="img/brand/5.png" alt="">
        </div>
        <div class="single_brand">
            <img src="img/brand/6.png" alt="">
        </div>
    </div>
    
</section>


<!-- blog::start  -->
<section class="blog_area grayBg">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex align-items-end mb_30 flex-wrap">
                <div class="section__title flex-fill">
                    <span class="subTitle">RESOURCES</span>
                    <h3 class="mb-0">Browse our Resource Center</h3>
                </div>
                <a href="blog.php" class="theme_line_btn">ALL ARTICLES</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="blog_widget mb_30">
                    <a href="blog_details.php" class="thumb">
                        <img class="img-fluid" src="img/blog/blog_img_1.png" alt="">
                    </a>
                    <div class="blog_meta">
                        <div class="blog_meta_top">
                            <a href="blog_details.php">
                            <span class="blog_meta_left">RESOURCE</span>
                            </a>
                            <span class="blog_meta_right">JANUARY 14, 2021</span>
                        </div>
                        <div class="blog_meta_inner">
                            <h4>
                                <a href="blog_details.php">
                                    8 TIPS IN CHOOSING THE RIGHT LAW FIRM FOR YOUR CASE AND NEEDS
                                </a>
                            </h4>
                            <p>Prepared by experienced English teachers, the texts, articles and convers
                                ations are brief and appropriate to your level of proficiency. Take the
                                multiple-choice quiz following each text, and</p>
                            <a class="theme_underLine_btn" href="blog_details.php">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="blog_widget mb_30">
                    <a href="blog_details.php" class="thumb">
                        <img class="img-fluid" src="img/blog/blog_img_2.png" alt="">
                    </a>
                    <div class="blog_meta">
                        <div class="blog_meta_top">
                            <a href="blog_details.php">
                                <span class="blog_meta_left">RESOURCE</span>
                            </a>
                            <span class="blog_meta_right">JANUARY 14, 2021</span>
                        </div>
                        <div class="blog_meta_inner">
                            <h4>
                                <a href="blog_details.php">
                                    8 TIPS IN CHOOSING THE RIGHT LAW FIRM FOR YOUR CASE AND NEEDS
                                </a>
                            </h4>
                            <p>Prepared by experienced English teachers, the texts, articles and convers
                                ations are brief and appropriate to your level of proficiency. Take the
                                multiple-choice quiz following each text, and</p>
                            <a class="theme_underLine_btn" href="blog_details.php">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- blog::end  -->


<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>