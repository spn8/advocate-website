
    <!-- Add Modal Add_color -->
    <div class="modal fade lms_view_modal" id="lms_view_modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal_header d-flex align-items-center gap_15 ">
                    <div class="modal_header_left flex-fill ">
                        <h4 class="modal-title">Fall Long Weekend boarding</h4>
                        <span class="edit_pop_text">Feb 14, 2022 - 09:00 AM</span>
                    </div>
                    <a href="#" class="link_icon">
                        <img src="img/link_icon.svg" alt="">
                    </a>
                </div>

                <div class="modal-body">
                    <div class="modal_bg">
                        <img class="img-fluid" src="img/modal_banner.jpg" alt="">
                    </div>
                    <p class="description_text">Aenean pretium convallis lorem, sit amet dapibus ante mollis 
                        Integer bibendum interdum sem, eget volutpat purus pulvinar
                        insed tristique augue vitae sagittis porta.</p>
                    <p class="description_text">Aenean pretium convallis lorem, sit amet dapibus ante mollis 
                        Integer bibendum interdum sem.</p>
                    <div class="modal_author_info">
                        <div class="thumb">
                            <img src="img/modal_author.png" alt="">
                        </div>
                        <div class="modal_author_content">
                            <a href="#">
                                <h4>ROBERT DOWNEY</h4>
                            </a>
                            <p>UI/UX Designer</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!--/ Add_Milestone -->
    <!-- UP_ICON  -->
    <div id="back-top" style="display: none;">
        <a title="Go to Top" href="#">
            <i class="ti-angle-up"></i>
        </a>
    </div>
    <!--/ UP_ICON -->

    <!--ALL JS SCRIPTS -->
    <script src="js/vendor/jquery-3.4.1.min.js"></script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/barfiller.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/parallax.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/vendor/calender_js/core/main.css"></script>
    <script src="js/vendor/calender_js/core/main.js"></script>
    <script src="js/vendor/calender_js/daygrid/main.js"></script>
    <script src="js/vendor/calender_js/timegrid/main.js"></script>
    <script src="js/vendor/calender_js/interaction/main.js"></script>
    <script src="js/vendor/calender_js/list/main.js"></script>
    <script src="js/vendor/calender_js/activation.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7nx22ZmINYk9TGiXDEXGVxghC43Ox6qA"></script>
    <script src="js/map.js"></script>
    <!-- MAIN JS   -->
    <script src="js/main.js"></script>

</body>

</html>