    <!-- FOOTER::START  -->
    <footer>
        <div class="footer_top_area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="box_footerBox d-flex justify-content-center ">
                            <div class="box_footerBox_inner d-flex align-items-center flex-column">
                                <a href="index.php">
                                    <img class="img-fluid" src="img/logo_white.png" alt="">
                                </a>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmodte
                                mpor incididunt ut labore et dolore magna aliqua. Quis ipsum .</p>
                                <div class="d-flex footerBox_socialLinks ">
                                    <a href="#">
                                        <i class="fab fa-facebook-f"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                    <a href="#">
                                        <i class="fab fa-instagram"></i>
                                    </a>
                                </div>
                                <div class="d-flex footer_menuLinks ">
                                    <a href="about.php">ABOUT US</a>
                                    <a href="practice_area.php">PRACTICE AREAS</a>
                                    <a href="case_result.php">CASE RESULTS</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>Company</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="about.php">About</a></li>
                                <li><a href="#">Career</a></li>
                                <li><a href="contact.php">Contact Form</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <div class="footer_widget">
                            <div class="footer_title">
                                <h3>Products</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="#">Education Application</a></li>
                                <li><a href="#">SP Page Builder</a></li>
                                <li><a href="#">Learning Management (LMS)</a></li>
                                <li><a href="#">Flutter App</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>Supports</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="#">Documentation</a></li>
                                <li><a href="#">Forums</a></li>
                                <li><a href="case_result.php">FAQs</a></li>
                                <li><a href="case_result.php">Support Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>Terms</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="#">Terms of Use</a></li>
                                <li><a href="#">Copyright Notice</a></li>
                                <li><a href="#">Refund Policy</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>Newsletter</h3>
                            </div>
                            <p>Don’t miss any updates of our new templates and
                            extensions and all the astonishing offers we bring for
                            you.</p>
                            <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                                <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                    method="get" class="subscription relative">
                                    <input name="EMAIL" class="form-control" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address'"
                                        required="" type="email">
                                    <div style="position: absolute; left: -5000px;">
                                        <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                    </div>
                                    <button class="">Subscribe</button>
                                    <div class="info"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright_bottom">
            <p>© 2022 <a href="#">Infix Advocate.</a> All rights reserved. Made By <a href="#">CodeThemes.</a></p>
        </div>
    </footer>
    <!-- FOOTER::END  -->