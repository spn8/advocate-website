    <!-- HEADER::START -->
    <header>
        <div id="sticky-header" class="header_area">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="col-12">
                        <div class="header__wrapper">
                            <!-- header__left__start  -->
                            <div class="header__left d-flex align-items-center">
                                <div class="logo_img">
                                    <a href="index.php">
                                        <img src="img/logo.png" alt="">
                                    </a>
                                </div>
                            </div>
                            <!-- header__left__start  -->
    
                            <!-- main_menu_start  -->
                            <div class="main_menu text-right d-none d-lg-block">
                                <nav>
                                    <ul id="mobile-menu">
                                        <li><a href="index.php">Home</a></li>
                                        <li><a href="About.php">About</a></li>
                                        <li><a href="#">Services</a></li>
                                        <li><a href="#">Team</a></li>
                                        <li><a href="boog.php">News</a></li>
                                        <li><a href="#">Pages</a>
                                            <ul class="submenu">
                                                <li><a href="index.php">Home</a></li>
                                                <li><a href="about.php">About</a></li>
                                                <li><a href="contact.php">Contact</a></li>
                                                <li><a href="blog.php">Blog</a></li>
                                                <li><a href="blog.php">Blog Post</a></li>
                                                <li><a href="packages.php">Packages</a></li>
                                                <li><a href="package_single.php">Packages Single</a></li>
                                                <li><a href="case_result.php">Case Result</a></li>
                                                <li><a href="case_result_details.php">Single Case Result</a></li>
                                                <li><a href="practice_area.php">Practice Areas</a></li>
                                                <li><a href="error_page.php">404 Not Found</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="contact.php">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- main_menu_start  -->
    
                            <!-- header__right_start  -->
                            <div class="header__right">
                                <div class="contact_wrap d-flex align-items-center">
                                    <div class="contact_btn d-none d-lg-block">
                                        <div class="primary_btn small_btn2">
                                            <a href="login.php">Login</a>
                                            <a href="resiter.php"> / Register</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- header__right_end  -->
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--/ HEADER::END -->