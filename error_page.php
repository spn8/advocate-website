<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<div class="error_wrapper blackOverlay">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-12">
                <div class="error_wrapper_info text-center">
                    <h1 class="error_code">404</h1>
                    <h3>Page Not Found</h3>
                    <p>Prepared by experienced English teachers, the texts, articles and
                        conversations are brief and appropriate to your level of proficiency. Take the </p>
                    <div class="d-flex align-items-center gap_r_15 gap_c_30 flex-wrap justify-content-center">
                        <a class="primary_btn" href="#">CONTACT ME</a>
                        <a class="primary_btn" href="#">GO HOME</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>