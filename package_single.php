<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<div class="pricing_details_banner bradcam_bg_1">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="pricing_details_wrapper ">
                    <div class="pricing_details_text">
                        <h3>Enterprise</h3>
                        <p>Access to 60 hours of legal consultation
                            with our firm for one month.</p>
                        <ul>
                            <li>60 hours of legal consultation</li>
                            <li>1 hour response time</li>
                            <li>Dedicated associate</li>
                            <li>Premium service & assistance</li>
                        </ul>
                    </div>
                    <div class="pricing_details_form">
                        <h4>GET YOUR PACKAGE TODAY</h4>
                        <p>Please select your package duration.
                            There are options for 1, 3, 6, and 12
                            months.</p>
                        <div class="priseText d-flex align-items-center gap_10 flex-wrap">
                            <h5 class="mb-0">$ 10,000.00 USD</h5>
                            <span>$ 12,000.00 USD</span>
                        </div>
                        <input class="primary_input mb_30" type="text" placeholder="SELECT PACKAGE DURATION">
                        <a href="#" class="primary_btn w-100 text-center">ADD TO CART</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="pricing_detailsArea">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="pricing_details_info">
                    <h3 class="mb_20">About this package</h3>
                    <p class="mb_15">Prepared by experienced English teachers, the texts, articles and conversations 
                    are brief and appropriate to your level of proficiency. Take the multiple-choice 
                    quiz following each text, and you'll get the results immediately. You will feel both 
                    challenged and accomplished! You can even</p>
                    <p class="mb-0">Et nunc, tellus sed arcu duis suspendisse magna id. At eros, vivamus sed donec
                    tincidunt elementum molestie volutpat. Ipsum lacinia viverra condimentum sit.
                    Blandit viverra mauris commodo risus nisi, egestas amet. Vestibulum varius nunc,
                    quisque imperdiet risus.</p>
                    <h4>IS THIS THE RIGHT PACKAGE FOR MY COMPANY?</h4>
                    <p>Prepared by experienced English teachers, the texts, articles and conversations 
                    are brief and appropriate to your level of proficiency. Take the multiple-choice 
                    quiz following each text, and you'll get the results immediately.</p>
                    <ul>
                        <li>Convallis tellus id interdum velit laoreet. Sapien et ligula ullamcorper malesuada</li>
                        <li>Sed cras ornare arcu dui vivamus arcu felis bibendum ut</li>
                        <li>Urna cursus eget nunc scelerisque viverra mauris in aliquam. Posuere ac ut cons</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="pricing_details_img">
                    <img class="img-fluid" src="img/packageThumb.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>