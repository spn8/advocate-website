<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<div class="blogDetailsBanner_area blackOverlay">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="blogBradcam_info">
                    <div class="d-flex gap_20 align-items-center">
                        <span>RESOURCE</span>
                        <div class="lineDivider style6"></div>
                        <h5>JANUARY 14, 2021</h5>
                    </div>
                    <h3>8 tips in choosing the right law
                        firm for your case and needs</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="blogDetailsPage_area  grayBg">
    <div class="container">
        <div class="borderBottom_1px">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="blogDetailsPage_img"></div>
                </div>
                <div class="col-xl-8">
                    <div class="blogPage_textInformation">
                        <div class="single_blogPage_textInformation mb_45">
                            <h3 class="mb_25">Why to choose the right law firm</h3>
                            <p>English texts for beginners to practice reading and comprehension online and free. Practicing your comprehension
                            of written English will both improve your vocabulary and understanding of grammar and word order. The texts bel
                            ow are designed to help you develop while giving you an instant evaluation of your progress.</p>
                        </div>
                        <div class="single_blogPage_textInformation">
                            <h4>HOW TO SEARCH FOR THE RIGHT LAW FIRM</h4>
                            <p class="mb_30">English texts for beginners to practice reading and comprehension online and for free. Practicing your
                                comprehension of written English will both improve your vocabulary and understanding grammar and word order.
                                The texts below are designed to help you develop while giving you an instant evaluation of your progress.
                                Prepared by experienced English teachers, the texts.</p>
                                <ul>
                                    <li>Aliquet enim tortor at auctor. Nisl pretium fusce id velit ut. Pharetra convallis Prepared by experienced  the
                                        texts,articles and conversations are brief and appropriate.</li>
                                    <li>Quisque non tellus orci ac auctor augue mauris. In iaculis nunc sed augu. Prepared by exprienced teachers.</li>
                                    <li>Tellus at urna condimentum mattis pellentesque id nibh. Consequat mauris Prepared by exprienced English
                                    teachers, the texts.</li>
                                </ul>
                                <div class="single_blogPageBanner">
                                    <img class="img-fluid" src="img/blog/blogPage.jpg" alt="">
                                </div>
                        </div>
                        <div class="single_blogPage_textInformation mb_30">
                            <h4>HOW TO FIND REAL REFERENCES AND REVIEWS</h4>
                            <p class="mb_20">Ac tincidunt vitae semper quis. Est pellentesque elit ullamcorper dignissim crasPrepared by experienced English
                                teachers, the texts, articles and conversations are brief and appropriate to your level of proficiency.
                                pellentesque habitant.</p>
                            <ol>
                                <li>01. Aliquet enim tortor at auctor. Nisl pretium fusce id velit ut. Pharetra exprienced English teachers, the texts,
                                quiz following each text, andposuere.</li>
                                <li>02. Quisque non tellus orci auctor augue mauris. iaculis nunc sed augu. Pharetra exprienced English teachers,</li>
                                <li>03. Tellus at urna condimentum mattis pellentesque id nibh. Consequat mauris  Pharetra exprienced English
                                    nunc congue.</li>
                            </ol>
                        </div>
                        <div class="single_blogPage_textInformation mb_30">
                            <h4>HOW TO FIND REAL REFERENCES AND REVIEWS</h4>
                            <p class="mb_30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Justo, euismod felis, eros mi  Pharetra exprienced English
                            purus in est. Vitae sodales tellus vitae, tincidunt in sed orci. Blandit proin posuere  Pharetra exprienced teachers,
                            vitae. Nisi in in nam viverra libero. Tincidunt viverra congue morbi ut fermentum. Id.</p> 
                            <div class="quote_text">
                                <p>“Ac tincidunt vitae semper quis. Est pelltesque elit Prepared exprienced English
                                    teachers, the texts, articles and conversations are brief and appopriate to your
                                    level of proficiency. Take the multiple-choice quiz following each text, and
                                    pellentesque habitant.”</p>
                            </div>                     
                        </div>
                        <div class="single_blogPage_textInformation">
                            <h4>WRAPPING UP THE ARTICLE</h4>
                            <p>Tellus at urna condimentum mattis pellentesque id nibh. Consequat mauris  Pharetra exprienced English teachers,
                            congue nisi vitae suscipit tellus mauris. Elementum integer enim the neque volutpat ac massa tempor nec feugiat
                            tincidunt. Lobortis mattis aliquam faucibus purus in massa tempor nec feugiat. Mauris cursus turpis massa ticidnt
                            ultrices eros in cursus turpis massa tincidunt. Ut tellus elementum sagittis vitae et leocursus turpis massa 
                            is ut diam.</p>
                            <div class="aboutInfoBox gap_20 mt_30">
                                <div class="thumb">
                                    <img class="img-fluid" src="img/blog/jhonImg.jpg" alt="">
                                </div>
                                <div class="aboutInfoBox_content">
                                    <span>JOHN CARTER</span>
                                    <h5>CORPORATE LAWYER</h5>
                                    <p>Prepared by experienced English are teachers, the texts, articles and conversation are
                                    and approprite to your level of prficiency. Take the multiple-choice quiz following each 
                                    and leocursus turpis massa is ut diam.</p>
                                    <a href="#" class="line_btn">ABOUT ME</a>
                                </div>
                                
                            </div>                    
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<!-- blog::start  -->
<section class="blog_area grayBg">
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex align-items-center mb_30 flex-wrap gap_20">
                <div class="section__title flex-fill">
                    <h3 class="mb-0 theme_text">Latest Articles</h3>
                </div>
                <a href="#" class="theme_line_btn">ALL ARTICLES</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="blog_widget mb_30">
                    <a href="#" class="thumb">
                        <img class="img-fluid" src="img/blog/blog_img_1.png" alt="">
                    </a>
                    <div class="blog_meta">
                        <div class="blog_meta_top">
                            <a href="#">
                            <span class="blog_meta_left">RESOURCE</span>
                            </a>
                            <span class="blog_meta_right">JANUARY 14, 2021</span>
                        </div>
                        <div class="blog_meta_inner">
                            <h4>
                                <a href="#">
                                    8 TIPS IN CHOOSING THE RIGHT LAW FIRM FOR YOUR CASE AND NEEDS
                                </a>
                            </h4>
                            <p>Prepared by experienced English teachers, the texts, articles and convers
                                ations are brief and appropriate to your level of proficiency. Take the
                                multiple-choice quiz following each text, and</p>
                            <a class="theme_underLine_btn" href="#">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="blog_widget mb_30">
                    <a href="#" class="thumb">
                        <img class="img-fluid" src="img/blog/blog_img_1.png" alt="">
                    </a>
                    <div class="blog_meta">
                        <div class="blog_meta_top">
                            <a href="#">
                                <span class="blog_meta_left">RESOURCE</span>
                            </a>
                            <span class="blog_meta_right">JANUARY 14, 2021</span>
                        </div>
                        <div class="blog_meta_inner">
                            <h4>
                                <a href="#">
                                    8 TIPS IN CHOOSING THE RIGHT LAW FIRM FOR YOUR CASE AND NEEDS
                                </a>
                            </h4>
                            <p>Prepared by experienced English teachers, the texts, articles and convers
                                ations are brief and appropriate to your level of proficiency. Take the
                                multiple-choice quiz following each text, and</p>
                            <a class="theme_underLine_btn" href="#">READ MORE</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- blog::end  -->


<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>