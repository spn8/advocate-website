<?php include 'include/header.php' ?>

<div class="login_wrapper">
    <div class="login_wrapper_right position-relative">
        <div class="login_main_info">
            <div class="logo">
                <a href="index.php">
                    <img src="img/logoWhite.png " alt="">
                </a>
            </div>
            <h4>Nice to see you again</h4>
            <h3>WELCOME BACK</h3>
            <div class="lineDivider style9 mb_20"></div>
            <p>Consistent quality and experience across
            all platforms and devices.</p>
        </div>
    </div>
    <div class="login_wrapper_left">
        <div class="login_wrapper_leftInner">
            <div class="login_wrapper_content">
                <h4>Sign In</h4>
                <p class="loginSubText">See your growth and get consulting support!</p>
                <div class="socail_links">
                    <a href="#" class="d-flex align-items-center justify-content-center">
                        <div class="icon">
                            <svg id="search" xmlns="http://www.w3.org/2000/svg" width="32.706" height="32.706" viewBox="0 0 32.706 32.706">
                                <path id="Path_739" data-name="Path 739" d="M7.246,149.2l-1.138,4.248-4.159.088a16.375,16.375,0,0,1-.12-15.264h0l3.7.679,1.622,3.681a9.756,9.756,0,0,0,.092,6.568Z" transform="translate(0 -129.432)" fill="#fbbb00"/>
                                <path id="Path_740" data-name="Path 740" d="M277.33,208.176a16.34,16.34,0,0,1-5.827,15.8h0l-4.664-.238-.66-4.121a9.743,9.743,0,0,0,4.192-4.975h-8.741v-6.467h15.7Z" transform="translate(-244.909 -194.87)" fill="#518ef8"/>
                                <path id="Path_741" data-name="Path 741" d="M55.14,318.745h0a16.352,16.352,0,0,1-24.632-5l5.3-4.336a9.722,9.722,0,0,0,14.01,4.978Z" transform="translate(-28.561 -289.639)" fill="#28b446"/>
                                <path id="Path_742" data-name="Path 742" d="M53.577,3.763,48.281,8.1a9.721,9.721,0,0,0-14.331,5.09l-5.325-4.36h0A16.35,16.35,0,0,1,53.577,3.763Z" transform="translate(-26.796)" fill="#f14336"/>
                            </svg>
                        </div>
                        <span>Google</span>
                    </a>
                    <a href="#" class="d-flex align-items-center justify-content-center">
                        <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="32.706" height="32.706" viewBox="0 0 32.706 32.706">
  <path id="facebook" d="M23.736,7.758A16.206,16.206,0,0,1,31.991,10a16.518,16.518,0,0,1,4.131,24.95,16.662,16.662,0,0,1-9.228,5.51V28.709H30.1l.725-4.62H25.97V21.064a2.629,2.629,0,0,1,.559-1.737,2.557,2.557,0,0,1,2.052-.781h2.933V14.5q-.063-.02-1.2-.161a23.807,23.807,0,0,0-2.584-.161,6.456,6.456,0,0,0-4.638,1.654,6.393,6.393,0,0,0-1.743,4.746v3.511h-3.7v4.62h3.7V40.464a16.262,16.262,0,0,1-10-5.51A16.5,16.5,0,0,1,15.481,10a16.211,16.211,0,0,1,8.255-2.246Z" transform="translate(-7.383 -7.758)" fill="#1877f2" fill-rule="evenodd"/>
</svg>

                        </div>
                        <span>Google</span>
                    </a>
                    <a href="#" class="d-flex align-items-center justify-content-center">
                        <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="40.254" height="32.706" viewBox="0 0 40.254 32.706">
                        <path id="twitter" d="M40.254,51.872a17.2,17.2,0,0,1-4.755,1.3,8.206,8.206,0,0,0,3.63-4.561,16.492,16.492,0,0,1-5.233,2,8.252,8.252,0,0,0-14.275,5.643,8.5,8.5,0,0,0,.191,1.882A23.359,23.359,0,0,1,2.8,49.5a8.255,8.255,0,0,0,2.536,11.03A8.15,8.15,0,0,1,1.61,59.518v.091a8.29,8.29,0,0,0,6.612,8.109,8.236,8.236,0,0,1-2.164.272A7.3,7.3,0,0,1,4.5,67.848,8.331,8.331,0,0,0,12.207,73.6a16.582,16.582,0,0,1-10.232,3.52A15.457,15.457,0,0,1,0,77a23.233,23.233,0,0,0,12.66,3.7c15.186,0,23.488-12.579,23.488-23.483,0-.365-.013-.717-.03-1.067A16.463,16.463,0,0,0,40.254,51.872Z" transform="translate(0 -48)" fill="#03a9f4"/>
                        </svg>

                        </div>
                        <span>Google</span>
                    </a>
                </div>
                <div class="loginSeperator d-flex align-items-center justify-content-between">
                    <span></span>
                    <p class="login_text">or Sign in with Email</p>
                    <span></span>
                </div>
                    
                    <form action="#">
                        <div class="row">
                            <div class="col-xl-12">
                                <label for="#" class="primary_label2 mb_15">Email Address <span>*</span></label>
                                <input class="primary_input4 mb_25" type="text" placeholder="Type e-mail address">
                            </div>
                            <div class="col-xl-12">
                                <label for="#" class="primary_label2 mb_15">Password <span>*</span></label>
                                <input class="primary_input4 mb_30" type="text" placeholder="Min. 8 Character">
                            </div>
                            <div class="col-12 mb_30">
                                <a href="#" class="primary_btn text-center w-100">Sign In</a>
                            </div>
                        </div>
                    </form>
                    <h5 class="shitch_text">Don’t have an account? <a href="resiter.php">Sing Up</a></h5>
            </div>
        </div>
    </div>
    
</div>

<?php include 'include/footer.php' ?>