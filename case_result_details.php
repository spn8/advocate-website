<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>
<div class="breadcrumb_area bradcam_bg_1 style5">
    <div class="container">
        <div class="row justify-content-center ">
            <div class="col-lg-10">
                <div class="breadcam_wrap2 align-items-center">
                    <div class="breadcam_wrapInner">
                        <h3>Corporate & Compliance</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit. Purus gravida in ipsum in
                        quis. Metus amet et risus platea.</p>
                        <a class="theme_underLine_btn" href="#">READ MORE</a>
                    </div>
                    <div class="thumb">
                        <img class="img-fluid" src="img/case/resultImg.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- caseResult_details_overview::start  -->
<div class="caseResult_details_overview">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="caseResult_details_header">
                    <div class="caseResult_details_headerSingle">
                        <h4>
                        DATE
                        </h4>
                        <p>Jan 2021</p>
                    </div>
                    <div class="caseResult_details_headerSingle">
                        <h4>
                        SERVICES
                        </h4>
                        <p class="serviceText">Corporate & Compliance Mergers & Acquisitions</p>
                    </div>
                    <div class="caseResult_details_headerSingle">
                        <h4>
                        CATEGORY
                        </h4>
                        <p>Business Taxation</p>
                    </div>
                    <div class="caseResult_details_headerSingle">
                        <h4>
                        RESULTS
                        </h4>
                        <p class="theme_text res_text">$46,000,000</p>
                    </div>
                </div>
                <div class="caseResult_details_body">
                    <div class="caseResult_details_single">
                        <div class="row">
                            <div class="col-xl-4">
                                <h4 class="mb_30">Overview</h4>
                            </div>
                            <div class="col-xl-8">
                                <p class="mb_25">Prepared by experienced English teachers, the texts, articles and conversations are
                                brief and appropriate to your level of proficiency. Take the multiple-choice quiz follow
                                each text, and you'll get the results immediately. You will feel both challenged and
                                accomplished.</p>
                                <p>Et nunc, tellus sed arcu duis suspendisse magna id. At eros, vivamus sed donec
                                tincidunt elementum molestie volutpat. Ipsum lacinia viverra condimentum sit. Blandit
                                viverra mauris commodo risus nisi, egestas amet. Vestibulum varius nunc, quisque
                                imperdiet risus.</p>
                            </div>
                        </div>
                    </div>
                    <div class="caseResult_details_single">
                        <div class="row">
                            <div class="col-xl-4">
                                <h4 class="mb_30">Results</h4>
                            </div>
                            <div class="col-xl-8">
                                    <p class="mb_25">Prepared by experienced English teachers, the texts, articles and conversations are
                                    brief and appropriate to your level of proficiency. Take the multiple-choice quiz follow
                                    each text, and you'll get the results immediately. You will feel both challenged and
                                    (as PDF) and print the texts and exercises.</p>
                                    <p>Et nunc, tellus sed arcu duis suspendisse magna id. At eros, vivamus sed donec
                                    tincidunt elementum molestie volutpat. Ipsum lacinia viverra condimentum sit. Blandit
                                    viverra mauris commodo risus nisi, egestas amet. Vestibulum varius nunc, quisque
                                    imperdiet risus.</p>
                                    <h5>LEGAL RESULTS</h5>
                                    <p class="mb_25">Prepared by experienced English teachers, the texts, articles and conversations are
                                    brief and appropriate to your level of proficiency. Take the multiple-choice quiz follow
                                    each text, and you'll get the results immediately. </p>
                                    <ul>
                                        <li>Sodales ut eu sem integer vitae. Lobortis mattis aliquam faucibus purus.
                                    Posuere ac</li>
                                    <li>Amet nulla facilisi morbi tempus iaculis. Turpis massa sed</li>
                                    <li>Nulla malesuada pellentesque elit eget gravida. Et malesuada fames ac turpis</li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="caseResultBox mb_30">
                    <h4>Results</h4>
                    <p class="mb_25">Prepared by experienced English teachers, the texts, articles and conversations.Prepared by experienced English teachers, 
                    are brief and appropriate to your level of proficiency. Take the multiple-choice Prepared by experienced English teachers, 
                    quiz following each text, and you'll get the results immediately. You will feel both Prepared by experienced English 
                    challenged and accomplished</p>
                    <p>Et nunc, tellus sed arcu duis suspendisse magna id. At eros, vivamus sed donec by experienced English teachers, the texts,
                    tincidunt elementum molestie volutpat. Ipsum lacinia viverra condimentum sit. Prepared by experienced English teachers
                    Blandit viverra mauris commodo risus nisi, egestas amet. Vestibulum varius nunc, Prepared by experienced articles and 
                    quisque imperdiet risus.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- caseResult_details_overview::end  -->

<section class="cta_area style2">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="section__title white_text text-start d-flex flex-column justify-content-start align-items-start">
                    <div class="lineDivider style7 mb_15"></div>
                    <h3>Get a Free Consultation</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
                        pellentesque adipiscing cras nec orci lacinia amet, vulputate.</p>
                    <div class="d-flex gap_30 flex-wrap">
                        <a href="#" class="primary_btn">CONTACT US</a>
                        <a href="#" class="theme_line_btn style2">(800) 567 - 1783</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="section_spacing_large">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="row">
                    <div class="col-12 d-flex align-items-end mb_30 flex-wrap gap_30">
                        <div class="section__title flex-fill">
                            <h3 class="mb-0 theme_text3">More Case Results</h3>
                        </div>
                        <a href="#" class="theme_line_btn">CASE RESULTS</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6 col-md-6">
                        <div class="caseResult_widget mb_30">
                            <div class="thumb">
                                <img class="img-fluid" src="img/case/case_img1.png" alt="">
                            </div>
                            <div class="caseResult_meta">
                                <span>$46,000,000</span>
                                <h4>
                                    <a href="#">
                                    CORPORATE & COMPLIANCE
                                    </a>
                                </h4>
                                <p>Prepared by experinced English teachers, texts, articles
                                and conversations are brief and appropriate to your 
                                level of proficiency. </p>
                                <a class="theme_underLine_btn style2" href="#">READ MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-md-6">
                        <div class="caseResult_widget mb_30">
                            <div class="thumb">
                                <img class="img-fluid" src="img/case/case_img2.png" alt="">
                            </div>
                            <div class="caseResult_meta">
                                <span>$46,000,000</span>
                                <h4>
                                    <a href="#">
                                    CORPORATE & COMPLIANCE
                                    </a>
                                </h4>
                                <p>Prepared by experinced English teachers, texts, articles
                                and conversations are brief and appropriate to your 
                                level of proficiency. </p>
                                <a class="theme_underLine_btn style2" href="#">READ MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>