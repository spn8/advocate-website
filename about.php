<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<div class="breadcrumb_area bradcam_bg_1 style6">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcam_wrap">
                    <span class="subtext">ABOUT US</span>
                    <h3>The Law Is The <span>Public</span> Conscience</h3>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="about_pageArea">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="about_pageAreaWrapper">
                    <div class="about_pageAreaWrapperLeft">
                        <h3>Aliquam Lacinia Ligula</h3>
                        <div class="single_aboutInfo">
                            <p>Maecenas a metus feugiat, tincidunt ex cursus, porta elit. Aenean eleifend vel elit a
                            dignissim. Aliquam lacinia ligula sit amet leo venenatis tempus. Sed quis orci nec odio
                            sollicitudin tempor sed ac mauris. In nulla purus, pulvinar at sollicitudin vel, semper vitae
                            nibh. Vestibulum feugiat ligula a luctus gravida. Sed eu mauris non orci maximus ornare ac
                            vitae massa. Aenean sodales arcu lobortis nisl tristique, malesuada commodo dui
                            commodo. Donec in purus vehicula, lobortis felis quis, aliquam odio.</p>
                        </div>
                        <div class="single_aboutInfo">
                            <h4>Proin vel ante et enim gravida</h4>
                            <p>Curabitur fermentum, ex sagittis pulvinar tincidunt, orci tortor pellentesque tortor, nec
                            vulputate enim magna eget erat. Nulla ultricies ultrices pretium. Nulla quis consectetur
                            ante. Nam et sagittis risus, at posuere enim. Cras semper, elit non gravida interdum, nisi
                            ligula maximus eros, eget consectetur est quam sit amet purus. Donec vulputate pretium
                            arcu, non feugiat sapien sollicitudin eu. Aenean sed vestibulum mi.</p>
                        </div>
                        <div class="single_aboutInfo">
                            <h4>Proin lorem sapien, placerat at vulputate</h4>
                            <p>Curabitur fermentum, ex sagittis pulvinar tincidunt, orci tortor pellentesque tortor, nec
                            vulputate enim magna eget erat. Nulla ultricies ultrices pretium. Nulla quis consectetur
                            ante. Nam et sagittis risus, at posuere enim. Cras semper, elit non gravida interdum, nisi
                            ligula maximus eros, eget consectetur est quam sit amet purus. Donec vulputate pretium
                            arcu, non feugiat sapien sollicitudin eu. Aenean sed vestibulum mi.</p>
                        </div>
                        <div class="single_aboutInfo">
                            <h4>Nullam posuere fermentum leo</h4>
                            <p>Curabitur fermentum, ex sagittis pulvinar tincidunt, orci tortor pellentesque tortor, nec
                            vulputate enim magna eget erat. Nulla ultricies ultrices pretium. Nulla quis consectetur
                            ante. Nam et sagittis risus, at posuere enim. Cras semper, elit non gravida interdum, nisi
                            ligula maximus eros, eget consectetur est quam sit amet purus. Donec vulputate pretium
                            arcu, non feugiat sapien sollicitudin eu. Aenean sed vestibulum mi.</p>
                        </div>
                    </div>
                    <div class="about_pageAreaWrapperImg">
                        <img class="img-fluid" src="img/about/aboutImg.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="counter_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4">
                <div class="single_counter mb_30">
                    <h3> <span class="counter" >252</span><span class="plus_icon ti-plus"></span></h3>
                    <h4>SUCCESSFUL CASES</h4>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="single_counter mb_30">
                    <h3> <span class="counter" >175</span><span class="plus_icon ti-plus"></span></h3>
                    <h4>SATISFIED CLIENTS</h4>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="single_counter mb_30">
                    <h3> <span class="counter" >18</span><span class="plus_icon ti-plus"></span></h3>
                    <h4>LAWYER ASSOCIATES</h4>
                </div>
            </div>
            <div class="col-lg-3 col-md-4">
                <div class="single_counter mb_30">
                    <h3> <span class="counter">252</span><span class="textIcon">M</span><span class="plus_icon ti-plus"></span></h3>
                    <h4>MONEY SAVED FOROUR CLIENTS</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="workArea grayBg section_spacing">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="section__title2 white_text text-center d-flex flex-column justify-content-center align-items-center mb_40">
                    <div class="lineDivider style8  mb_5"></div>
                    <span class="subTitle mb_10">VALUES</span>
                    <h3 class="theme_text3 max_490px">The core values behind all my work</h3>
                </div>
                <div class="about_boxs">
                    <div class="about_single_box">
                        <div class="icon">
                            <img src="img/about/icon_1.svg" alt="">
                        </div>
                        <div class="aboutInfo_text">
                            <h4>EXCELLENCE</h4>
                            <p>Prepared by experienced English teachers,
                            the texts, articles and conversations
                            are brief and appropriate.</p>
                        </div>
                    </div>
                    <div class="about_single_box">
                        <div class="icon">
                            <img src="img/about/icon_2.svg" alt="">
                        </div>
                        <div class="aboutInfo_text">
                            <h4>TRUST</h4>
                            <p>Prepared by experienced English teachers,
                            the texts, articles and conversations
                            are brief and appropriate.</p>
                        </div>
                    </div>
                    <div class="about_single_box">
                        <div class="icon">
                            <img src="img/about/icon_3.svg" alt="">
                        </div>
                        <div class="aboutInfo_text">
                            <h4>INTEGRITY</h4>
                            <p>Prepared by experienced English teachers,
                            the texts, articles and conversations
                            are brief and appropriate.</p>
                        </div>
                    </div>
                    <div class="about_single_box">
                        <div class="icon">
                            <img src="img/about/icon_4.svg" alt="">
                        </div>
                        <div class="aboutInfo_text">
                            <h4>ACCOUNTABILITY</h4>
                            <p>Prepared by experienced English teachers,
                            the texts, articles and conversations
                            are brief and appropriate.</p>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center mb_20">
                    <a href="contact.php" class="primary_btn">CONTACT ME</a>
                </div>
                
            </div>
        </div>
    </div>
</div>

<!-- TESTMONIAL::START  -->
<div class="testmonial_area2 grayBg">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10 d-flex align-items-end mb_80 flex-wrap gap_20">
                <div class="section__title flex-fill">
                    <h3 class="mb-0 theme_text3">Great past results for our clients</h3>
                    <p>Prepared by experienced English teachers, the texts, articles and conversations
                    are brief and appropriate to your level of proficiency.</p>
                </div>
                <a href="contact.php" class="primary_btn">CONTACT ME</a>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <div class="testmonail_active owl-carousel">
                    <div class="testmonial_widget mb_30">
                        <p>“Prepared by experienced English teachers, the texts, art
                            icles and conversations are brief and appropriate to your
                            level of proficiency. Take the multiple-choice quiz following
                            each text, you'll get the results immediately.“</p>
                        <div class="testmonial_bottom d-flex align-items-center">
                            <div class="thumb">
                                <img class="img-fluid" src="img/about/testimonial_1.png" alt="">
                            </div>
                            <div class="reviewer_name">
                                <h4>SOPHIE WATERS</h4>
                                <h5>CEO AT GLOBAL BRANDS</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TESTMONIAL::END  -->



<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>